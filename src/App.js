import React from "react"
import './App.css';

///////  Home Work - 1
// import Homework  from './homework_1/GoodsApp';

///////  Home Work - 2
import Homework  from './homework_2/BlogApp';


class App extends React.Component {

  render() {
    return (
      <div className="App">
        <div className="container">
          <Homework />
        </div>
      </div>
    );
  };
}

export default App;