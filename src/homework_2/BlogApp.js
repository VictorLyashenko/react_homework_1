import React, { Component } from 'react'
import PostList  from './components/PostList';
import AuthorInfo  from './components/AuthorInfo';
import './BlogApp.css';

export default class BlogApp extends Component {

    state = {
        posts:[],
        currentAuthorId: 1
    }

    onSelectPost = id => {
        this.setState({ currentAuthorId: id })
    }

    render() {
        return (
            <div className="container">
                <div  className="row d-flex justify-content-around">
                    <div className="col-md-5">
                        <PostList onSelectPost={this.onSelectPost}/>
                    </div>
                    <div className="col-md-5">
                        <AuthorInfo setUser={this.state.currentAuthorId}/>       
                    </div>
                </div>
            </div>
        )
    }
}