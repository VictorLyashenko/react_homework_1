import React, { Component } from 'react'
import Loading from "./Loading";
import BlogService from "./BlogService.js";
import './AuthorInfo.css';

class AuthorInfo extends Component {

    blogService = new BlogService();

    state = {
        userList: [],
        loading: false
    };

    componentDidMount() {
        this.updateAuthor()
    }

    componentDidUpdate(prefProps){
        if (this.props.setUser !== prefProps.setUser) {
        this.updateAuthor();
        }
    }

    updateAuthor() {
        this.setState({ loading: true });
        const id = this.props.setUser
        this.blogService
        .getUser(id)
        .then((userList) => {
            this.setState({
                userList,
                loading: false
            });
        });
    }

    render () {
        const {name, email, website} = this.state.userList
        const { loading } = this.state;

        if(loading) {
            return <Loading />
        }
        return (
            <div className="author_info">
                <p className="list-group-item active author_name">
                    {name}
                </p>
                <p className="list-group-item author_email">
                    {email}
                </p>
                <p className="list-group-item ">
                    {website}
                </p>
            </div>
        )
    }    
}

export default AuthorInfo;