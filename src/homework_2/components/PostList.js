import React, { Component } from 'react';
import BlogService from "./BlogService";
import Loading from "./Loading";
import Post from './Post'
import './PostList.css';

class PostList extends Component {

    blogService = new BlogService();

    state = {
        posts:[],
        loading: false
    }

    componentDidMount() {
        this.setState({ loading: true });
        this.blogService
            .getAllPosts()
            .then((posts) => {
                this.setState({
                    posts,
                    loading: false
                });
            });
    }

    renderPosts() {
        return this.state.posts.map(post =>
            <Post
              key={post.id} 
              post={post}
              onSelectPost={this.props.onSelectPost}
            />
        ) 
    }
  
    render() {
        const { loading } = this.state;

        if(loading) {
            return <Loading />
        }
        return (
            <div className="post_list">
                { this.renderPosts() }
            </div>
        )
    }
}

export default PostList ;
