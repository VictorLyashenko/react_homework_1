import React, { Component } from 'react'

export default class Post extends Component {

    render() {
        const { email, body } = this.props.data;

        return (
             <div className="list-group-item list-group-item-action">
                <a href="##">{email}</a>
                <p>{body[0].toUpperCase() + body.slice(1)}</p>
            </div> 
        )
    }
}
