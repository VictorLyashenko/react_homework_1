import React, { Component } from 'react'
import BlogService from "./BlogService";
import Comment from "./Comment";
import Loading from "./Loading";

export default class Post extends Component {

    blogService = new BlogService();

    state = {
        comments:[],
        loading: false
    }

    componentDidMount() {
        this.updateComment()
    }

    componentDidUpdate(prefProps){
        if (this.props.postId !== prefProps.postId) {
        this.updateComment();
        }
    }

    updateComment = () => {
        this.setState({ loading: true });
        const id = this.props.postId 
        this.blogService
        .getComments(id)
        .then((comments) => {
            this.setState({
                comments,
                loading: false
            });
        });
    }

    renderComment() {
        return this.state.comments.map(data =>
          <Comment
            key={data.id} 
            data={data}
          />
        )
      }

    render() {
        const { loading } = this.state;

        if(loading) {
            return <Loading />
        }
        return (
            <div>
                { this.renderComment() }
            </div> 
        )
    }
}
