import React, { Component } from 'react'
import Comments from "./Comments";

export default class Post extends Component {

    state = {
        comments:[],
        showBody: false
    }

    onShowBody = () => {
        const userId = this.props.post.userId
        this.props.onSelectPost(userId)
        this.setState((state) => {
            return {
                showBody: !state.showBody,
            }
        });
    }

    render() {
        const { showBody } = this.state;
        const { body, title } = this.props.post
        
        return (
            <div>
                <h6 onClick={this.onShowBody} className="list-group-item list-group-item-action active" >
                    {title[0].toUpperCase() + title.slice(1)}
                </h6>
                {showBody && <div className="list-group-item list-group-item-action disabled">
                    {body[0].toUpperCase() + body.slice(1)}
                    <Comments postId={this.props.post.userId}/>
                </div>}
            </div>
        )
    }
}