export default class BlogService {

  _apiBase = 'https://jsonplaceholder.typicode.com'

  getResource = async (url) => {
    const res = await fetch(`${this._apiBase}${url}`);

    if (!res.ok) {
      throw new Error(`Could not fetch ${url}` +
        `, received ${res.status}`)
    }
    return await res.json();
  }

  getAllPosts = async () => {
    const posts = await this.getResource(`/posts/`);
    return posts
  }

  getUser = async (id) => {
    const user = await this.getResource(`/users/${id}/`);
    return user
  }
  
  getComments = async (id) => {
    const comments = await this.getResource(`/posts/${id}/comments`);
    return comments
  }
}