import React, { Component } from 'react';


class AddProductForm  extends Component {
  
  state = {
    productsName: '',
    category: '',
    price: '',
    balance: '',
}

  onChange = (e, item) => {
    this.setState({
      [item]: e.target.value
    })
  }

  onSubmit = () => {
    if(this.state.productsName !== '' && this.state.category !== '' && this.state.price !== '' && this.state.balance !== '') {
      this.props.onAddProduct(this.state)
      this.setState(
      { 
        productsName: '',
        category: '',
        price: '',
        balance: '',
      }
    )
    }else{
      alert('Заполните все поля!')
    }
  }
  
  render() {
    return (
      <div className="AddProductForm">
        <input type="text" placeholder="Название товара" value={this.state.productsName} onChange={e => this.onChange(e, 'productsName')} />
        <input type="text" placeholder="Категория" value={this.state.category} onChange={e => this.onChange(e, 'category')} />
        <input type="number" placeholder="Цена" value={this.state.price} onChange={e => this.onChange(e, 'price')} />
        <input type="number" placeholder="Остаток" value={this.state.balance} onChange={e => this.onChange(e, 'balance')} />
        <button className="addProduct"  onClick={this.onSubmit} >Добавить товар</button>
      </div>
    )
  }
}

export default AddProductForm;