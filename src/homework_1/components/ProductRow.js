import React, { Component } from 'react'
import classes from './ProductRow.module.css';


class ProductRow  extends Component {
    render() {
        const { productsName, id, category, price, balance } = this.props.data;
        return (
            <div >
                <table border="1">
                    <tbody>
                        <tr>
                            <td>{productsName}</td>
                            <td>{category}</td>
                            <td>{price}</td>
                            <td>{balance}</td>
                            <td>
                                <button className={classes.removeButton} onClick={() => this.props.onProductRemove(id, productsName)}>Удалить</button>
                            </td>
                        </tr> 
                    </tbody>
                </table>
            </div>
        )
    }
}

export  default ProductRow ;
