import React, { Component } from 'react'
import './ProductTable.module.css';

class ProductTable extends Component {
    render(){
        return(
        <table border="1">
            <tbody>
                <tr>
                    <th>Название</th>
                    <th>Категория</th>
                    <th>Цена</th>
                    <th>Остаток</th>
                    <th>Действие</th>
                </tr>
            </tbody>
        </table>
        )
    }     
}

export default ProductTable;