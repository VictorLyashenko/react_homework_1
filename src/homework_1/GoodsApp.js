import React from "react"
import ProductRow  from './components/ProductRow';
import ProductTable from "./components/ProductTable";
import AddProductForm from "./components/AddProductForm";
import './BlogApp.css';


class App extends React.Component {

  state = {
    products:[]
  }

  onProductRemove = (id, productsName) => {
    const productsUpdated = this.state.products.filter(product => product.id !== id);
    this.setState({ products: productsUpdated})
    alert('Вы удалили товар: ' + productsName)
  }

  renderProducts() {
    return this.state.products.map(product =>
      <ProductRow
        key={product.id} 
        onProductRemove={this.onProductRemove}
        data={product}
      />
    )
  }

  addProduct = (newProduct) => {
    const { products } = this.state;
    const updateProducts = [
      ...products,
      {
        id: "key" + Math.random(),
        ...newProduct
      }
    ]
    this.setState(
      { 
        products: updateProducts
      }
    )
  }

  render() {
    return (
      <div>
        
        <ProductTable />
      
        {this.renderProducts()}
        
        <AddProductForm onAddProduct={this.addProduct} />
      
      </div>
    )
  }
}

export default App;